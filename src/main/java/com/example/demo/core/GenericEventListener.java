package com.example.demo.core;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class GenericEventListener {

    private static final Map<Class<?>, Class<?>> primitiveWrapper = new HashMap<Class<?>, Class<?>>();

    static {
        primitiveWrapper.put(Integer.class, int.class);
        primitiveWrapper.put(Long.class, long.class);
        primitiveWrapper.put(Float.class, float.class);
        primitiveWrapper.put(Double.class, double.class);
        primitiveWrapper.put(Boolean.class, boolean.class);
        primitiveWrapper.put(Byte.class, byte.class);
        primitiveWrapper.put(Short.class, short.class);
        primitiveWrapper.put(Character.class, char.class);
    }

    private final ApplicationContext context;

    @EventListener
    public void performDynamicTasks(GenericEvent event){

        Map<String, ClassAndMethod> map = event.getMap();
        map.forEach((key, value)->{
            Object bean = context.getBean(value.getAClass());
            try {
                Method method = findMethod(bean, value.getMethodName(), value.getParameters());
                method.invoke(bean,value.getParameters() == null ? new Object[]{value.getParameters()} : value.getParameters());
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });

    }

    private Method findMethod(Object tested, String methodToExecute, Object[] arguments){

        if(tested == null){
            throw new IllegalArgumentException("The object to perform the operation on cannot be null");
        }

        Class<?> clazz = null;
        if(isClass(tested)){
            clazz = (Class<?>) tested;
        } else {
            clazz = tested.getClass();
        }

        Method[] methods = null;
        methods = ReflectionUtils.getUniqueDeclaredMethods(clazz);

        Method potentialMethodToInvoke = null;
        for (Method method : methods) {
            if(method.getName().equals(methodToExecute)){
                Class<?>[] paramTypes = method.getParameterTypes();
                if(arguments != null && (paramTypes.length == arguments.length)){
                    if (paramTypes.length == 0) {
                        potentialMethodToInvoke = method;
                        break;
                    }
                    boolean methodFound = checkArgumentTypesMatchParameterTypes(method.isVarArgs(), paramTypes, arguments);
                    if(methodFound){
                        return method;
                    }
                }
                 else if (arguments == null && paramTypes.length == 1 && !paramTypes[0].isPrimitive()) {
                    potentialMethodToInvoke = method;
                }
            }
        }

        return potentialMethodToInvoke;
    }

    private boolean checkArgumentTypesMatchParameterTypes(boolean isVarArgs, Class<?>[] parameterTypes, Object[] arguments) {
        if (!isVarArgs && arguments.length != parameterTypes.length) {
            return false;
        }
        for (int i = 0; i < arguments.length; i++) {
            Object argument = arguments[i];
            if (argument == null) {
                final int index;
                if (i >= parameterTypes.length) {
                    index = parameterTypes.length - 1;
                } else {
                    index = i;
                }
                final Class<?> type = parameterTypes[index];
                if (type.isPrimitive()) {
                    // Primitives cannot be null
                    return false;
                }
            } else if (i >= parameterTypes.length) {
                if (!isAssignableFrom(parameterTypes[parameterTypes.length - 1], getType(argument))) {
                    return false;
                }
            } else {
                boolean assignableFrom = isAssignableFrom(parameterTypes[i], getType(argument));
                final boolean isClass = parameterTypes[i].equals(Class.class) && isClass(argument);
                if (!assignableFrom && !isClass) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isAssignableFrom(Class<?> parameter, Class<?> argument) {
        boolean assignableFrom;
        assignableFrom = parameter.isAssignableFrom(argument);
        if (!assignableFrom && hasPrimitiveCounterPart(argument)) {
            final Class<?> primitiveFromWrapperType = getPrimitiveFromWrapperType(argument);
            if (primitiveFromWrapperType != null) {
                assignableFrom = parameter.isAssignableFrom(primitiveFromWrapperType);
            }
        }
        return assignableFrom;
    }

    private Class<?> getComponentType(Class<?> type) {
        Class<?> theType = type;
        while (theType.isArray()) {
            theType = theType.getComponentType();
        }
        return theType;
    }

    private Class<?>[] getTypes(Object[] arguments) {
        Class<?>[] classes = new Class<?>[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            classes[i] = getType(arguments[i]);
        }
        return classes;
    }

    private Class<?> getType(Object object) {
        Class<?> type = null;
        if (isClass(object)) {
            type = (Class<?>) object;
        } else if (object != null) {
            type = object.getClass();
        }
        return type;
    }

    private boolean isClass(Object argument) {
        return argument instanceof Class<?>;
    }

    private Class<?> getPrimitiveFromWrapperType(Class<?> wrapperType) {
        return primitiveWrapper.get(wrapperType);
    }

    private boolean hasPrimitiveCounterPart(Class<?> type) {
        return primitiveWrapper.containsKey(type);
    }
}
