package com.example.demo.core;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.HashMap;
import java.util.Map;

@Getter
public class GenericEvent<T> extends ApplicationEvent {
    private T object;
    Map<String, ClassAndMethod> map = new HashMap<>();

    public GenericEvent(Object source,
                        T object,
                        Map<String, ClassAndMethod> map) {
        super(source);
        this.object = object;
        this.map = map;
    }
}
