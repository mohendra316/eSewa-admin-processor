package com.example.demo.core;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ClassAndMethod {
    private Class<?> aClass;
    private String methodName;
    private Object[] parameters;
}
