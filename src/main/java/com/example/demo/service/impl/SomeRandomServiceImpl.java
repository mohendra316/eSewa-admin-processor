package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

@Service
public class SomeRandomServiceImpl implements SomeRandomService {
    @Override
    public void test() {
        System.out.println("random service");
    }
}
