package com.example.demo.service.impl;

public interface MinioService {
    void uploadJSON();
    void uploadJSON(int name);
    void varArgsMethod(String ...name);
    void varArgsMethod(String name);
}
