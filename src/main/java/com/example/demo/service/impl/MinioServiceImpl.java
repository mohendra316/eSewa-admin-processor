package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

@Service
public class MinioServiceImpl implements MinioService {

    private final SomeRandomService someRandomService;

    public MinioServiceImpl(SomeRandomService someRandomService) {
        this.someRandomService = someRandomService;
    }

    @Override
    public void uploadJSON(){
        System.out.println("JSON uploaded");
        someRandomService.test();
//        test(1);
    }

    @Override
    public void uploadJSON(int name) {
        System.out.println("JSON for "+name+" uploaded");
    }

    @Override
    public void varArgsMethod(String... name) {
        someRandomService.test();
        System.out.println("VAR_ARGS "+name);
    }

    @Override
    public void varArgsMethod(String name) {
        System.out.println("NOOO "+name);
    }

    private void test(int i){
        System.out.println("TEST for private method inside publid");
    }
}
