package com.example.demo.service.processor;

import com.example.demo.service.TaskEnum;

import java.util.Set;

public interface AdminCentralProcessor<T, K> {
    Set<TaskEnum> getSupportedTypes();
    T process(K input);
    void processForDelegate();
}
