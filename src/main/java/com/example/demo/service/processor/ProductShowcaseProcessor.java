package com.example.demo.service.processor;

import com.example.demo.core.ClassAndMethod;
import com.example.demo.core.GenericEvent;
import com.example.demo.delegate.AdminCentralBeanProcessor;
import com.example.demo.delegate.AdminCentralBroker;
import com.example.demo.delegate.AdminCentralService;
import com.example.demo.delegate.AdminDelegateDto;
import com.example.demo.service.TaskEnum;
import com.example.demo.service.impl.MinioServiceImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Service
public class ProductShowcaseProcessor implements ApplicationEventPublisherAware,AdminCentralProcessor{
    private ApplicationEventPublisher publisher;
    private final AdminCentralBroker adminCentralBroker;
    private final AdminCentralBeanProcessor beanFactory;

    public ProductShowcaseProcessor(AdminCentralBroker adminCentralBroker,
                                    AdminCentralBeanProcessor beanFactory) {
        this.adminCentralBroker = adminCentralBroker;
        this.beanFactory = beanFactory;
    }

    @Override
    public Set<TaskEnum> getSupportedTypes() {
        return new HashSet<>(Collections.singletonList(TaskEnum.SHOWCASE));
    }

    @Override
    public Object process(Object input) {
        int i=9;
        System.out.println("Test for processor for showcase");
        Map<String, ClassAndMethod> map = new HashMap<>();
        map.put("minio", ClassAndMethod.builder()
                .aClass(MinioServiceImpl.class)
                .methodName("varArgsMethod")
                .parameters(new Object[]{new String[]{"mohit","mmmmm"}})
                .build());
        GenericEvent<?> genericEvent = new GenericEvent<>(this, null, map);
        publisher.publishEvent(genericEvent);
        return null;
    }

    @Override
    public void processForDelegate() {
        String[] serviceCodes  = {"cache", "minio"};
        Map<String, AdminDelegateDto> map = new HashMap<>();
        getProcessorMap(serviceCodes, map);
        adminCentralBroker.performTasks(map,false);
    }

    private void getProcessorMap(String[] serviceCodes, Map<String, AdminDelegateDto> map) {
        Stream.of(serviceCodes)
                .forEach(code ->{
                    AdminDelegateDto beanAndInputDto = new AdminDelegateDto();
                    beanAndInputDto.setAdminCentralService(beanFactory.getAdminCentralBean(getAdminServiceBeanName(code)));
                    if(code.equals("minio")) {
                        beanAndInputDto.setInput(23);
                    }
                    if(code.equals("cache")) {
                        beanAndInputDto.setInput(new Integer[]{1,2,2});
                    }
                    map.put(code, beanAndInputDto);
                });
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    private String getAdminServiceBeanName(String serviceCode) {
        return "showcase"+"_"+serviceCode;
    }
}
