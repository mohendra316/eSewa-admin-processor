package com.example.demo.service.processor;

import com.example.demo.service.TaskEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class TaskManagerImpl implements TaskManager {
    private final Map<TaskEnum, AdminCentralProcessor<?,?>> taskProcessor = new HashMap<>();

    @Autowired
    public TaskManagerImpl(List<AdminCentralProcessor<?,?>> factories) {
        factories.forEach((processor) -> {
            processor.getSupportedTypes().forEach((type) -> {
                        this.taskProcessor.put(type, processor);
                    }
            );
        });
    }

    @Override
    public AdminCentralProcessor newInstance(final String code) {
            return taskProcessor.get(TaskEnum.valueOf(code));
    }
}