package com.example.demo.service.processor;

public interface TaskManager {
    AdminCentralProcessor newInstance(String code);
}
