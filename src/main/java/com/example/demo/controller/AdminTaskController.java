package com.example.demo.controller;

import com.example.demo.service.TaskEnum;
import com.example.demo.service.processor.AdminCentralProcessor;
import com.example.demo.service.processor.TaskManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminTaskController {

    private final TaskManager taskManager;


    public AdminTaskController(TaskManager taskManager) {
        this.taskManager = taskManager;
    }

    @GetMapping("/api/test")
    public void performAdminActions() {
        AdminCentralProcessor processor = taskManager.newInstance(TaskEnum.SHOWCASE.name());
        processor.process(null);
    }

    @GetMapping("/api/delegate")
    public void performTasks() {

        AdminCentralProcessor processor = taskManager.newInstance(TaskEnum.SHOWCASE.name());
        processor.processForDelegate();
    }
}
