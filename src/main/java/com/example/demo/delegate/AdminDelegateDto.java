package com.example.demo.delegate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminDelegateDto {
    private boolean success;
    private AdminCentralService<?,?> adminCentralService;
    private Object input;
    private Object output;
}
