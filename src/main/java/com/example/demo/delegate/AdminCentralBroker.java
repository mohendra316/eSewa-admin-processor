package com.example.demo.delegate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

@Service
public class AdminCentralBroker implements TaskBroker {

    public AdminCentralBroker() {
        System.out.println("Admin central broker constructor called");
    }

    @Override
    public void performTasks(Map<String, AdminDelegateDto> map ,boolean continueOnError){
        for(Map.Entry<String, AdminDelegateDto> entry: map.entrySet()){
            Type genericInput;
            Class<?> inputArgument = null;
            Type[] genericInterfaces = entry.getValue().getAdminCentralService().getClass().getGenericInterfaces();
            for (Type genericInterface : genericInterfaces) {
                if (genericInterface instanceof ParameterizedType) {
                    genericInput = ((ParameterizedType) genericInterface).getActualTypeArguments()[0];
                    inputArgument = (Class<?>) genericInput;
                }
            }
            try{
                Object response = entry.getValue().getAdminCentralService().processRequest(Objects.isNull(inputArgument) ? null : convertInstanceOfObject(entry.getValue().getInput(), inputArgument.getName()));
                entry.getValue().setOutput(response);
                entry.getValue().setSuccess(true);
            }catch (Exception exe){
                entry.getValue().setOutput(exe);
                if(!continueOnError) {
                    break;
                }
            }
        }
    }

    private <T> T convertInstanceOfObject(Object o, String className) {
        try {
            Class<T> clazz = (Class<T>) Class.forName(className);
            return clazz.cast(o);
        } catch(ClassNotFoundException e) {
            return null;
        }
    }
}
