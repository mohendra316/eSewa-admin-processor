package com.example.demo.delegate.delegateImpl.showcase;

import com.example.demo.delegate.AdminCentralService;
import org.springframework.stereotype.Service;

@Service("showcase_cache")
public class ShowcaseCacheEvict implements AdminCentralService<Integer[], Object> {

    @Override
    public Object processRequest(Integer[] input) {
        System.out.println("array: "+ input.toString());

        throw new IllegalArgumentException("Cache exception");
    }
}
