package com.example.demo.delegate.delegateImpl.resource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShowCaseResource {
    private int id;
    private String product;
    private Banks banks;
}
