package com.example.demo.delegate.delegateImpl;

import com.example.demo.delegate.AdminCentralService;
import com.example.demo.delegate.delegateImpl.resource.Banks;

import java.util.List;

public class SSBankMinioServiceImpl implements AdminCentralService<List<Banks>, Void> {
    @Override
    public Void processRequest(List<Banks> input) {
        System.out.println("Banks being uploaded to minIO");
        return null;
    }
}
