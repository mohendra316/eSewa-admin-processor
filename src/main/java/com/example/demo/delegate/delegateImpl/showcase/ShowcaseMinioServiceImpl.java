package com.example.demo.delegate.delegateImpl.showcase;

import com.example.demo.delegate.AdminCentralService;
import com.example.demo.delegate.delegateImpl.resource.Banks;
import com.example.demo.delegate.delegateImpl.resource.ShowCaseResource;
import org.springframework.stereotype.Service;

@Service("showcase_minio")
public class ShowcaseMinioServiceImpl implements AdminCentralService<Integer, ShowCaseResource> {

    @Override
    public ShowCaseResource processRequest(Integer input) {
        System.out.println("Showcase being uploaded " + input);
        ShowCaseResource showCaseResource = new ShowCaseResource();
        showCaseResource.setId(1);
        showCaseResource.setProduct("Bank Payment");
        showCaseResource.setBanks(Banks.builder()
                .id(1)
                .swiftCode("GLBBNPKA")
                .displayName("Global IME Bank Ltd. ")
                .build());
        return showCaseResource;
    }
}
