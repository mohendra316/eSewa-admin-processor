package com.example.demo.delegate.delegateImpl;

import com.example.demo.delegate.AdminCentralService;

public class NEACacheEvict implements AdminCentralService {

    @Override
    public Object processRequest(Object input) {
        System.out.println("Cache of NEA being evicted");
        return null;
    }
}
