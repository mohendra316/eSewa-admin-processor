package com.example.demo.delegate.delegateImpl.resource;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Banks {
    private int id;
    private String swiftCode;
    private String displayName;
}
