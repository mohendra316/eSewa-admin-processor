package com.example.demo.delegate;

import java.util.Map;

public interface TaskBroker {

    void performTasks(Map<String, AdminDelegateDto> map ,boolean continueOnError);
}
