package com.example.demo.delegate;

public interface AdminCentralService<I,O> {
    O processRequest(I input) throws Exception;
}
