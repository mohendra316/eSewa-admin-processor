package com.example.demo.delegate;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Service;

@Service
public class AdminCentralBeanProcessor {

    private final BeanFactory beanFactory;

    public AdminCentralBeanProcessor(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public AdminCentralService<?,?> getAdminCentralBean(String beanName){
       return beanFactory.getBean(beanName, AdminCentralService.class);
    }
}
